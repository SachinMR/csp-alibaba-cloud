# Setup of Alibaba Cloud for ARM-CSP architecture

## Clone the repository
```
$ git clone -b <BRANCH_NAME> <PROJECT_GIT_URL>
```
Follow the below sequence to setup the services and containers.

## Setup Link IoT Edge service
- Follow document Link_IoT_Edge/docs/ARM_Smart_camera_CSP_Setting_Link_IoT_Edge.pdf for the setup of Link IoT Edge service.

## Setup the Apsara Live service for live streaming
- Follow document Cloud_container/docs/ARM-Smart_camera_CSP_Alibaba_Setting_up_Apsara_Live_and_Apsara_VOD_service.pdf for setup of Apsara Live service and getting the Live streaming ingest and pull URL.

## Build the cloud container
- Follow document cloud_container/docs/ARM-Smart_camera_CSP_Alibaba_Cloud_container_deployment.pdf for building and deploying the cloud container.

## Setup the Cloud monitor Dashboard on Alibaba cloud.
- Follow document Cloud_Monitor/docs/ARM-Smart_camera_CSP_Alibaba_Setting_up_Cloud_Monitor.pdf for setting the cloud monitor service and dashboard.

## Deploying all the containers using kubernetes 
- Follow document kubernetes/docs/ARM_Smart_camera_CSP_Deployment_on_kubernetes.pdf for deploying all the containers using kubernetes.
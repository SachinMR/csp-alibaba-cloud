import time 
from threading import Thread
import traceback

# Reporting the data to Alibaba Cloud monitoring using multiprocessing 
def report_data(cm_req, iot_req, iot_resp, cm_resp, client, group_id, debug=False): 
    s_time=time.time()                  
    print("Subprocess reporting data to cloud_monitor.....")           
    person_s = cm_resp["person_s"]                                     
    car_s = cm_resp["car_s"]                                           
    bicycle_s = cm_resp["bicycle_s"]                                   
    bus_s = cm_resp["bus_s"]                                           
    truck_s = cm_resp["truck_s"]                                       
    rest_inf_time_s = cm_resp["rest_inf_time_s"]                       
    pure_inference_time_s = cm_resp["pure_inference_time_s"]           
    try:
        cm_req.set_MetricLists([                                                                                                                                                          
            {"GroupId": group_id, "MetricName": "person", "Dimensions": "{\"Time\":\"x\",\"Detection_count\":\"y\"}",   
            "Type": "0", "Values": person_s                                                                                                     
            } ,  {"GroupId": group_id, "MetricName": "Bicycle", "Dimensions": "{\"Time\":\"x\",\"Detection_count\":\"y\"}",     
            "Type": "0", "Values": bicycle_s                                                                                                       
            } ,{"GroupId": group_id, "MetricName": "Car", "Dimensions": "{\"Time\":\"x\",\"Detection_count\":\"y\"}",         
            "Type": "0", "Values": car_s                                                                                                           
            }  , {"GroupId": group_id, "MetricName": "Bus", "Dimensions": "{\"Time\":\"x\",\"Detection_count\":\"y\"}",          
            "Type": "0", "Values": bus_s                                                                                                            
            } , {"GroupId": group_id, "MetricName": "Truck", "Dimensions": "{\"Time\":\"x\",\"Detection_count\":\"y\"}",
            "Type": "0", "Values": truck_s                                                                                                  
            } , {"GroupId": group_id, "MetricName": "End_to_End_inference_FPS", "Dimensions": "{\"Time\":\"x\",\"FPS\":\"y\"}",
            "Type": "0", "Values": rest_inf_time_s                                                                                                 
            } ,  {"GroupId": group_id, "MetricName": "Pure_inference_FPS", "Dimensions": "{\"Time\":\"x\",\"FPS\":\"y\"}",           
            "Type": "0", "Values": pure_inference_time_s                                                                                                
            }
            ])                                                                                                                                                                                                                                                                  
        response_m = client.do_action_with_exception(cm_req)

        iot_req.add_query_param('Items', iot_resp)
        response_m = client.do_action(iot_req)

        e_time = time.time()
        print("Subprocess reporting done----time taken : ", e_time - s_time)              
    except Exception as e:
        if debug:
            traceback.print_exc()
        print("Error while reporting")


def send_metrics_to_cloud(metrics, cm_req, iot_req, client, group_id):
    cm_resp = {"person_s": str({"value": metrics["person"]}),
            "car_s": str({"value": metrics["car"]}),
            "bus_s": str({"value": metrics["bus"]}),
            "bicycle_s": str({"value": metrics["bicycle"]}),
            "truck_s": str({"value": metrics["truck"]}),
            "rest_inf_time_s": str({"value": metrics["end_to_end_fps"]}),
            "pure_inference_time_s": str({"value":metrics["pure_fps"]})
            }

    iot_resp = str({"person": metrics["person"], "bicycle": metrics["bicycle"], "car": metrics["car"], "bus": metrics["bus"], 
                            "truck": metrics["truck"], "pure_fps": metrics["pure_fps"], "end_to_end_fps": metrics["end_to_end_fps"], 
                            "total_count": metrics["total_count"]})
    rep_data_process = Thread(target=report_data, args=(cm_req, iot_req, iot_resp, cm_resp, client, group_id))                                                                                    
    rep_data_process.start()            

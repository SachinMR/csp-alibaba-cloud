
def ArgConf(ap):
    ap.add_argument("-p", "--cloud_port", default="8081", help="Cloud container GRPC server port number") 
    ap.add_argument("-ls", "--rtmp_url", default="", help="Live streaming url from Apsara Video live") 
    args = vars(ap.parse_args())
    
    return args